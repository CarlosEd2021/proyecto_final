<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {
        HttpSession var_Session = request.getSession(false);
        String nombres = (String) var_Session.getAttribute("sessionNombres");
        String user = (String) var_Session.getAttribute("sessionUsuario");
        String tipo = (String) var_Session.getAttribute("sessionTipo");
        String correo = (String) var_Session.getAttribute("sessionEmail");

        if (user == null) {
            out.print("<center><h2><font color='blue'>Debe de haber iniciado Sesion para poder ingresar a esta pagina.</font><br><hr><font color='blue'>Intentelo de Nuevo</font><hr><h2></center><br>");
            out.print("<center><h2><font color='blue'> Por Favor Epere...</font><hr><h2></center>");
            //out.print("<meta http-equiv='refresh' content='4; url=http:sesion'/ >");
            //out.print("<meta http-equiv='refresh' content='4; url=http:./'/ >");
            response.sendRedirect("./");
        } else if (user != null) {
%>
<!DOCTYPE html>
<html>
    
    <head> 
        <title>Control de Inventario</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Incluimos la vista parcial la cual contiene las url de las librerias 
             correspondientes a estilos y js
        -->
        <style>
        *{
            padding: 0;
            margin: 0;
            
        }
        img{
      width: 50%;
    }
    body {
      background: url(assets/img/OIP.jpg);
      background-size: cover;
      background-repeat: no-repeat;
      margin: 0;
      height: 100vh;
    }
    .table{
        color: white;
        background: window;
    }
    </style>
        <%@include file="WEB-INF/Vistas-Parciales/css-js.jspf" %>
    </head>
    <body >
        <!-- Vistar parciales de la parte superior de nuestra aplicación -->
        <%@include file="WEB-INF/Vistas-Parciales/encabezado.jspf" %>

        <!-- Todo el contenido de la aplicación -->
        <!--<h3 style="text-align: center; width: 100%;">BIENVENID@S:<%= nombres%></h3>-->
        <div class="container">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">INTEGRANTES</th>
                        <th scope="col">CARNET</th>
                        <th scope="col">GRUPO</th>
                        <th scope="col">ROL</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Carlos Eduardo delcid Gaitán</td>
                        <td>218220</td>
                        <td>SIS21B</td>
                        <td>Scrum Master</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Ángel Daniel Valle Pineda</td>
                        <td>212920</td>
                        <td>SIS21A</td>
                        <td>Back-end</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Cristian Elí García Alvares</td>
                        <td>217620</td>
                        <td>SIS21B</td>
                        <td>Analista</td>
                    </tr>
                    <tr>
                        <th scope="row">4</th>
                        <td>Leonardo Misael Grande Pineda</td>
                        <td>212820</td>
                        <td>SIS21A</td>
                        <td>Front-end</td>
                    </tr>
                    <tr>
                        <th scope="row">5</th>
                        <td>Andreina Isabel Bonilla Chávez</td>
                        <td>210820</td>
                        <td>SIS21B</td>
                        <td>Diseñador BD</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- Vistar parciales de la parte inferior de nuestra aplicación -->
        <%@include file="WEB-INF/Vistas-Parciales/pie.jspf" %>
    </body>
</html>
<%
        }
        //Aca puede ir un mensaje para informar que no se ha iniciado sesión.
    } catch (Exception e) {
    System.out.println("error" +e);
    }
%>