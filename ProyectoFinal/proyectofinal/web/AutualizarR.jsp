<%@page import="java.sql.*"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
   try{
        HttpSession var_Session = request.getSession(false);
        String nombres = (String) var_Session.getAttribute("sessionNombres");
        String user = (String) var_Session.getAttribute("sessionUsuario");
        String tipo = (String) var_Session.getAttribute("sessionTipo");
        String correo = (String) var_Session.getAttribute("sessionEmail");
        if(user == null){
            response.sendRedirect("./");
        }else if(user!=null){
%>
<!DOCTYPE html>
<!-- El id debe ser el mismo que se le colocó de nombre a la sesión en el controlador -->
<jsp:useBean id="lista" scope="session" class="java.util.List" />

<html>
     
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <title>Control de Inventario</title>
        <style>
        *{
            padding: 0;
            margin: 0;
            
        }
        img{
      width: 50%;
    }
    body {
      background: url(assets/img/OIP.jpg);
      background-size: cover;
      background-repeat: no-repeat;
      margin: 0;
      height: 100vh;
    }
    .table{
        color: white;
        background: window;
    }
    </style>
        <%@include file="WEB-INF/Vistas-Parciales/css-js.jspf" %>
    </head>
    <body >    
        <%@include file="WEB-INF/Vistas-Parciales/encabezado.jspf"%>
        <%
       //CONECTANOD A LA BASE DE DATOS:
       Connection con;
       String url="jdbc:mysql://localhost:3306/bd_inventario";
       String Driver="com.mysql.jdbc.Driver";
       String usuario="root";
       String clave="";
       Class.forName(Driver);
       con=DriverManager.getConnection(url,usuario,clave);
       //Emnpezamos Listando los Datos de la Tabla Usuario pero de la fila seleccionada
       PreparedStatement ps;
       ResultSet rs;
       int id = Integer.parseInt(request.getParameter("id"));
       ps=con.prepareStatement("select * from tb_producto where id_producto="+id);
       rs=ps.executeQuery();
       while(rs.next()){
                         %>
        <div class="container" style="padding-left: 344px; padding-top: 30px">
            
            <form action="" method="post" class="form-control bg-dark" style="width: 400px; height: 520px; color: white">
                <h1>Modificar Registro</h1>
           <br>
                ID:
                <input type="text" readonly="" class="form-control" value="<%= rs.getInt("id_producto")%>"/>
                 NOMBRE PRODUCTO:
                <input type="text" name="txtnom" class="form-control" value="<%= rs.getString("nom_producto")%>"/>
                STOCK:
                <input type="text" name="txtstock" class="form-control" value="<%= rs.getString("stock")%>"/>
                PRECIO:
                <input type="text" name="txtprecio" class="form-control" value="<%= rs.getString("precio")%>"/>
                UNIDAD DE MEDIDA:
                <input type="text" name="txtud_medida" class="form-control" value="<%= rs.getString("unidad_de_medida")%>"/>
                ESTADO PRODUCTO:
                <input type="text" name="txtest_product" class="form-control" value="<%= rs.getString("estado_producto")%>"/>
                CATEGORIA:
                <input type="text" name="txtcat" class="form-control" value="<%= rs.getString("categoria")%>"/>
                FECHA ENTREGA
                <input type="text" name="txtfech_entrada" class="form-control" value="<%= rs.getString("fecha_entrada")%>"/>
                <hr>
                <input type="submit" value="Actualizar" class="btn btn-primary "/>
                  
                <a href="index1.jsp" class="btn btn-success ">Regresar</a>
            </form>
            <%}%>
        </div>
        <%@include file="WEB-INF/Vistas-Parciales/pie.jspf" %>
    </body>
</html>
<%
     String nom,stock,precio,ud_medida,est_produc,cat,feh_entrada;
     
                nom = request.getParameter("txtnom");
                stock = request.getParameter("txtstock");
                precio = request.getParameter("txtprecio");
                ud_medida = request.getParameter("txtud_medida");
                est_produc = request.getParameter("txtest_product");
                cat = request.getParameter("txtcat");
                feh_entrada = request.getParameter("txtfech_entrada");
               
                if (nom != null && stock != null && precio != null && ud_medida != null && est_produc!= null && cat!= null && feh_entrada!=null) {
          ps=con.prepareStatement("UPDATE tb_producto SET nom_producto='" + nom + "',stock='" + stock + "', precio='" + precio + "', unidad_de_medida='" + ud_medida + "', estado_producto='" + est_produc + "', categoria='" + cat + "',fecha_entrada='" + feh_entrada + "' where id_producto="+id );
            ps.executeUpdate();
           response.sendRedirect("index1.jsp");
       }
       
       
%>
<%
            }
               //Aca puede ir un mensaje para informar que no se ha iniciado sesión.
            }catch(Exception e){
                System.out.println("error" + e);
            }
%>