<%@page import="java.sql.*"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
   try{
        HttpSession var_Session = request.getSession(false);
        String nombres = (String) var_Session.getAttribute("sessionNombres");
        String user = (String) var_Session.getAttribute("sessionUsuario");
        String tipo = (String) var_Session.getAttribute("sessionTipo");
        String correo = (String) var_Session.getAttribute("sessionEmail");
        if(user == null){
            response.sendRedirect("./");
        }else if(user!=null){
%>
<!DOCTYPE html>
<!-- El id debe ser el mismo que se le colocó de nombre a la sesión en el controlador -->
<jsp:useBean id="lista" scope="session" class="java.util.List" />

<html>
     
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <title>Control de Inventario</title>
        <style>
        *{
            padding: 0;
            margin: 0;
            
        }
        img{
      width: 50%;
    }
    body {
      background: url(assets/img/OIP.jpg);
      background-size: cover;
      background-repeat: no-repeat;
      margin: 0;
      height: 100vh;
    }
    .table{
        color: white;
        background: window;
    }
    </style>
        <%@include file="WEB-INF/Vistas-Parciales/css-js.jspf" %>
    </head>
    <body >    
         <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>  
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <%@include file="WEB-INF/Vistas-Parciales/encabezado.jspf"%>
        <%

        Connection cone;
            String url = "jdbc:mysql://localhost:3306/bd_inventario";
            String Driver = "com.mysql.jdbc.Driver";
            String usuario = "root";
            String clave = "";
            Class.forName(Driver);
            cone = DriverManager.getConnection(url, usuario, clave);
            //Emnpezamos Listando los Datos de la Tabla Usuario
            PreparedStatement ps;
            Statement smt;
            ResultSet rs;
            smt = cone.createStatement();
            rs = smt.executeQuery("select * from tb_producto");
            //Creamo la Tabla:     
        %>
       
        <br>       
     
        <div class="container" >        
            <a href="insertarRegistros.jsp" class="btn btn-success btn-lg" >Nuevo Registro </a>
            <h3 style="text-align: center;">Listado de Productos Registradas</h3>
            <table class="table table-responsive"  id="tablaDatos" style="margin-top: 30px">
                    <thead>
                        <tr style="background: #a6e1ec">
                            <th class="text-center">ID</th>
                            <th class="text-center">NOMBRE PRODUCTO</th>
                            <th class="text-center">STOCK</th>
                            <th class="text-center">PRECIO</th>
                            <th class="text-center">UNIDAD DE MEDIDA</th>
                            <th class="text-center">ESTADO PRODUCTO</th>
                            <th class="text-center">CATEGORIA</th>
                            <th class="text-center">FECHA ENTRADA</th>
                            <th class="text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="tbodys">
                        <%
                            while (rs.next()) {
                        %>
                        <tr style="background: background;color: white">
                            <td class="text-center" style=" width:  115px"><%= rs.getInt("id_producto")%></td>
                            <td class="text-center" style=" width:  115px"><%= rs.getString("nom_producto")%></td>
                            <td class="text-center" style=" width:  115px"><%= rs.getString("stock")%></td>
                            <td class="text-center" style=" width:  115px"><%= rs.getString("precio")%></td>
                            <td class="text-center" style=" width:  115px"><%= rs.getString("unidad_de_medida")%></td>
                            <td class="text-center" style=" width:  115px"><%= rs.getString("estado_producto")%></td>
                            <td class="text-center" style=" width:  115px"><%= rs.getString("categoria")%></td>
                            <td class="text-center" style=" width:  115px"><%= rs.getString("fecha_entrada")%></td>
                            <td class="text-center" style=" width:  175px">
                                
                                
                                <a href="AutualizarR.jsp?id=<%= rs.getInt("id_producto")%>" class="btn btn-primary">Modificar</a>
                                <a href="delete.jsp?id=<%= rs.getInt("id_producto")%>" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                        <%}%>
                </table>
               
         
        </div>      
        <%@include file="WEB-INF/Vistas-Parciales/pie.jspf" %>
        </div>
         
    </body>
</html>


<%
            }
               //Aca puede ir un mensaje para informar que no se ha iniciado sesión.
            }catch(Exception e){
                System.out.println("error" + e);
            }
%>