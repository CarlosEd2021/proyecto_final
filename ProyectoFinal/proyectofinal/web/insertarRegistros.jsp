<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
   try{
        HttpSession var_Session = request.getSession(false);
        String nombres = (String) var_Session.getAttribute("sessionNombres");
        String user = (String) var_Session.getAttribute("sessionUsuario");
        String tipo = (String) var_Session.getAttribute("sessionTipo");
        String correo = (String) var_Session.getAttribute("sessionEmail");
        if(user == null){
            response.sendRedirect("./");
        }else if(user!=null){
%>
<!DOCTYPE html>
<!-- El id debe ser el mismo que se le colocó de nombre a la sesión en el controlador -->

<html>
     
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <title>Control de Inventario</title>
        <style>
        *{
            padding: 0;
            margin: 0;
            
        }
        img{
      width: 50%;
    }
    body {
      background: url(assets/img/OIP.jpg);
      background-size: cover;
      background-repeat: no-repeat;
      margin: 0;
      height: 100vh;
    }
    .table{
        color: white;
        background: window;
    }
    </style>
        <%@include file="WEB-INF/Vistas-Parciales/css-js.jspf" %>
    </head>
    <body >    
        <%@include file="WEB-INF/Vistas-Parciales/encabezado.jspf"%>
    
        <div><div class="container" style="padding-left: 344px; padding-top: 30px">         
          <form action="" method="post" class="form-control bg-dark" style="width: 400px; height: 470px; color: white">
                <h1>Agregar Registro</h1>
            <br>
                
                 NOMBRE PRODUCTO:
                <input type="text" name="txtnom" class="form-control">
                STOCK:
                <input type="text" name="txtstock" class="form-control">
                PRECIO:
                <input type="text" name="txtprecio" class="form-control">
                UNIDAD DE MEDIDA:
                <input type="text" name="txtud_medida" class="form-control">
                ESTADO PRODUCTO:
                <input type="text" name="txtest_product" class="form-control">
                <div class="form-group">
                            <label ><strong>CATEGORIA:</strong></label>
                            <select class="form-control" id="estado" name="feh">
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                            </select>
                          </div>
                
                <hr>
                <input type="submit" value="Agregar Nuevo Registro" class="btn btn-primary "/>
                <input type="button" class="btn btn-warning"  value="Consultar registros" onclick="window.location.href='index1.jsp'"/>
            </form></div> </div>
        <%@include file="WEB-INF/Vistas-Parciales/pie.jspf" %>
        
    </body>
</html>
<%
       //CONECTANOD A LA BASE DE DATOS:
       Connection con;
       String url="jdbc:mysql://localhost:3306/bd_inventario";
       String Driver="com.mysql.jdbc.Driver";
       String usuario="root";
       String clave="";
       Class.forName(Driver);
       con=DriverManager.getConnection(url,usuario,clave);
       PreparedStatement ps;
    
     String nom,stock,precio,ud_medida,est_produc,cat;
     
                nom = request.getParameter("txtnom");
                stock = request.getParameter("txtstock");
                precio = request.getParameter("txtprecio");
                ud_medida = request.getParameter("txtud_medida");
                est_produc = request.getParameter("txtest_product");
                cat = request.getParameter("txtcat");
                
               
                if (nom != null && stock != null && precio != null && ud_medida != null && est_produc!= null && cat!= null) {
                ps=con.prepareStatement("UPDATE tb_producto SET nom_producto='" + nom + "',stock='" + stock + "', precio='" + precio + "', unidad_de_medida='" + ud_medida + "', estado_producto='" + est_produc + "', categoria='" + cat + "' where id_producto="+id );

                    ps.executeUpdate();     
           response.sendRedirect("index1.jsp");
           
       }
       
       
%>
<%
            }
               //Aca puede ir un mensaje para informar que no se ha iniciado sesión.
            }catch(Exception e){
                System.out.println("error" + e);
            }
%>