-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-06-2021 a las 17:15:04
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_inventario`
--
CREATE DATABASE IF NOT EXISTS `bd_inventario` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bd_inventario`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_categoria`
--

DROP TABLE IF EXISTS `tb_categoria`;
CREATE TABLE IF NOT EXISTS `tb_categoria` (
  `id_categoria` int(5) NOT NULL AUTO_INCREMENT,
  `nom_categoria` varchar(50) NOT NULL,
  `estado_categoria` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tb_categoria`
--

INSERT INTO `tb_categoria` (`id_categoria`, `nom_categoria`, `estado_categoria`) VALUES
(1, 'Frutas', 1),
(2, 'Vegetales', 1),
(3, 'Mariscos', 1),
(4, 'Lacteos', 1),
(5, 'rwerw', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_producto`
--

DROP TABLE IF EXISTS `tb_producto`;
CREATE TABLE IF NOT EXISTS `tb_producto` (
  `id_producto` int(9) NOT NULL AUTO_INCREMENT,
  `nom_producto` varchar(50) NOT NULL,
  `stock` decimal(3,2) DEFAULT NULL,
  `precio` decimal(3,2) DEFAULT NULL,
  `unidad_de_medida` varchar(20) DEFAULT NULL,
  `estado_producto` tinyint(1) DEFAULT NULL,
  `categoria` int(5) DEFAULT NULL,
  `fecha_entrada` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_producto`),
  KEY `categoria` (`categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tb_producto`
--

INSERT INTO `tb_producto` (`id_producto`, `nom_producto`, `stock`, `precio`, `unidad_de_medida`, `estado_producto`, `categoria`, `fecha_entrada`) VALUES
(1, 'mansana', '1.00', '0.25', '1', 1, 1, '2021-06-27 21:03:16'),
(2, 'tomate', '2.00', '0.25', '2', 2, 2, '2021-06-27 21:03:16'),
(3, 'molusco', '3.00', '2.00', '3', 3, 3, '2021-06-27 21:03:16'),
(4, 'yogurt', '4.00', '0.25', '4', 4, 4, '2021-06-27 21:03:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_usuario`
--

DROP TABLE IF EXISTS `tb_usuario`;
CREATE TABLE IF NOT EXISTS `tb_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `apellido` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `correo` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `usuario` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `clave` varchar(150) CHARACTER SET utf8 NOT NULL,
  `tipo` tinyint(1) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `pregunta` varchar(60) CHARACTER SET utf8 NOT NULL,
  `respuesta` varchar(35) CHARACTER SET utf8 NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tb_usuario`
--

INSERT INTO `tb_usuario` (`id`, `nombre`, `apellido`, `correo`, `usuario`, `clave`, `tipo`, `estado`, `pregunta`, `respuesta`, `fecha_registro`) VALUES
(2, 'Carlos Eduardo', 'Delcid Gaitán', 'carlos@gmail.com', 'carlos', '827ccb0eea8a706c4c34a16891f84e7b', 1, 1, '¿Cuál es su color favorito?', 'blanco', '2021-06-26 18:40:45'),
(3, 'Ángel Daniel', 'Valle Pineda', 'daniel@gmail.com', 'daniel', '827ccb0eea8a706c4c34a16891f84e7b', 1, 1, '¿Cuál es su color favorito?', 'rojo', '2021-06-26 18:40:45'),
(4, 'Cristian Elí', 'García Alvares ', 'cristian@gmail.com', 'cristian', '827ccb0eea8a706c4c34a16891f84e7b', 1, 1, '¿Cuál es su color favorito?', 'verde', '2021-06-26 18:40:45'),
(5, 'Leonardo Misael', 'Grande Pineda ', 'leonardo@gmail.com', 'leonardo', '827ccb0eea8a706c4c34a16891f84e7b', 1, 1, '¿Cuál es su color favorito?', 'amarillo', '2021-06-27 02:16:50'),
(6, 'Andreina Isabel', 'Bonilla Chávez', 'andreina@gmail.com', 'andreina', '827ccb0eea8a706c4c34a16891f84e7b', 1, 1, '¿Cuál es su color favorito?', 'azul', '2021-06-27 02:17:01');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tb_producto`
--
ALTER TABLE `tb_producto`
  ADD CONSTRAINT `tb_producto_ibfk_1` FOREIGN KEY (`categoria`) REFERENCES `tb_categoria` (`id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
