/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;
import Model.Producto;

/**
 *
 * @author HP14
 */
public interface ProductoDAO {

    public List<Producto> Listar2();
    public Producto EditarProduc(int id_produc_edit);
    public boolean guardarProduc(Producto producto);
    public boolean borrarProduc(int produc_borrar);
}