/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Producto;
import Factory.ConexionBD;
import Factory.FactoryConexionBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
public class ProductoDAOImplementar implements ProductoDAO {
    ConexionBD conn;

    public ProductoDAOImplementar() {
        this.conn = FactoryConexionBD.open(FactoryConexionBD.MySQL);
    }

    @Override
    public List<Producto> Listar2() {
       this.conn = FactoryConexionBD.open(FactoryConexionBD.MySQL);
        StringBuilder miSQL = new StringBuilder();
        miSQL.append("select * FROM tb_producto;");
        List<Producto> lista2 = new ArrayList<>();
        try {
            ResultSet resultadoSQL = this.conn.consultaSQL(miSQL.toString());
            while (resultadoSQL.next()) {
                Producto Producto = new Producto();
                Producto.setId_producto(resultadoSQL.getInt("id_producto"));
                Producto.setNom_producto(resultadoSQL.getString("nom_producto"));
                Producto.setCategoria_id(resultadoSQL.getInt("categoria"));
                Producto.setStock(resultadoSQL.getFloat("stock"));
                Producto.setPrecio(resultadoSQL.getFloat("precio"));
                Producto.setUnidadMedida(resultadoSQL.getInt("unidad_de_Medida"));
                Producto.setEstado(resultadoSQL.getInt("estado_producto"));

                lista2.add(Producto);
            }
        } catch (SQLException ex) {
            System.out.println("error: " + ex);
        } finally {
            this.conn.cerrarConexion();
        }
        return lista2;
    }
    

    @Override
    public Producto EditarProduc(int id_produc_edit) {
       this.conn = FactoryConexionBD.open(FactoryConexionBD.MySQL);
        Producto Producto = new Producto();
        StringBuilder miSQL = new StringBuilder();
        
        miSQL.append("select *FROM tb_producto WHERE id_producto= ").append(id_produc_edit);
        try {
            ResultSet resultadoSQL = this.conn.consultaSQL(miSQL.toString());
            while (resultadoSQL.next()) {

                Producto.setId_producto(resultadoSQL.getInt("id_producto"));
                Producto.setNom_producto(resultadoSQL.getString("nom_producto"));
                Producto.setStock(resultadoSQL.getFloat("stock"));
                Producto.setPrecio(resultadoSQL.getFloat("categoria_id"));
                Producto.setUnidadMedida(resultadoSQL.getInt("unidad_de_medida"));
                Producto.setEstado(resultadoSQL.getInt("estado_producto"));

            }
        } catch (SQLException ex) {
            System.out.println("error" + ex);
        } finally {
            this.conn.cerrarConexion();
        }
        return Producto;

    }


    @Override
    public boolean borrarProduc(int produc_borrar) {
        this.conn = FactoryConexionBD.open(FactoryConexionBD.MySQL);
        boolean borrar = false;
        try {
            StringBuilder miSQL = new StringBuilder();
            miSQL.append("DELETE FROM tb_producto WHERE id_producto =").append(produc_borrar);
            this.conn.ejecutarSQL(miSQL.toString());
            borrar = true;
        } catch (Exception ex) {
            System.out.println("error" + ex);
        } finally {
            this.conn.cerrarConexion();
        }
        return borrar;
    }

    @Override
    public boolean guardarProduc(Producto producto) {
               this.conn = FactoryConexionBD.open(FactoryConexionBD.MySQL);
        boolean guardar = false;
        try{
            if(producto.getId_producto()== 0){
                StringBuilder miSQL = new StringBuilder();
                //Agregar consulta SQL; el id_categoria es autoincrementable.
                
               miSQL.append("INSERT INTO tb_producto(nom_producto,stock,precio,unidad_de_medida,estado_producto,categoria) Values('");
               miSQL.append(producto.getNom_producto()+ "','").append(producto.getStock()+ "','").append(producto.getPrecio()+ "','").append(producto.getUnidadMedida()+ "','").append(producto.getEstado()+ "','").append(producto.getCategoria_id());
                //Invocar método para ejecutar la consulta.
                this.conn.ejecutarSQL(miSQL.toString());
                System.out.println("Registro Guardado...");
                guardar = true;
                this.conn.ejecutarSQL(miSQL.toString());
            } else if (producto.getId_producto() > 0) {
                StringBuilder miSQL = new StringBuilder();
                miSQL.append("UPDATE INTO tb_producto SET id_producto = ").append(producto.getId_producto());
                miSQL.append(", nom_producto= ").append(producto.getNom_producto());
                miSQL.append(", stock = ").append(producto.getStock());
                miSQL.append(", precio = ").append(producto.getPrecio());
                miSQL.append(", unidadMedida = ").append(producto.getUnidadMedida());
                miSQL.append(", estado = ").append(producto.getEstado());
                miSQL.append(" WHERE id_producto = ").append(producto.getId_producto());
                this.conn.ejecutarSQL(miSQL.toString());
            }
            
        } catch (Exception ex) {
            System.out.println("error" + ex);
        } finally {
            this.conn.cerrarConexion();
        }
        return guardar;
    }

    }

    

    